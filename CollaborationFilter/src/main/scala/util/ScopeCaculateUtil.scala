package util

/**
  * Created by chenjianwen on 2016/3/7.
  */
object ScopeCaculateUtil {

  //根据时间给出一个评分比例
  def time2Score(time:Long):Double = {
    GlobalArgs.BASE_SCORE*time2Rate(time)
  }

  //根据分数给出基础比例
  def action2Rate(actType:Int):Double=actType match {
    case GlobalArgs.TYPE_CLICK=>1.3
    case GlobalArgs.TYPE_STORE1=>1.2
    case GlobalArgs.TYPE_STORE2=>0.5 //取消收藏
    case GlobalArgs.TYPE_SHARE=>1.5
    case GlobalArgs.TYPE_BOOK=>2
    case _ => 1
  }

  def time2Rate(time:Long):Double={
    val currentTimeInSec = System.currentTimeMillis()/1000;
    (currentTimeInSec - time) match{
      case a =>{
        if(a<=GlobalArgs.THREE_MONTH)
          return 1.0
        else if(a>GlobalArgs.THREE_MONTH && a<=GlobalArgs.SIX_MONTH)
          return 0.8
        else
          return 0.5
      }
    }
  }

  /**
    * @param list   ((userId,ProductId),ActionType,time) 传入的list的(userId,productId)需要全部一样
    * @return
    */
  def actions2Socrecompute(list: Iterable[(Int,Long)]): Double ={
    var extractScope = 0.0
    for(one <- list){
      extractScope = extractScope + 1*ScopeCaculateUtil.action2Rate(one._1)*ScopeCaculateUtil.time2Rate(one._2)
    }
    return extractScope+GlobalArgs.BASE_SCORE
  }
}
